# WarGames

This repository contains various notes and work on different WarGames/CTF
challenges I've completed.

**WARNING: These notes will contain spoilers and passwords!**

## Table Of Contents

- [Over The Wire](otw/)
  - [Bandit](otw/bandit.md)
  - [Leviathan](otw/leviathan.md)
  - [Natas](otw/natas.md)
