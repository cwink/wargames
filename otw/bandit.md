# Bandit

The following are the details for the
[Bandit](https://overthewire.org/wargames/bandit/) challenges.

**WARNING: These notes will contain spoilers and passwords!**

## Level 0

This one was simple, simply `cat` the readme file. Password is
`NH2SXQwcBdpmTEzi3bvBHMM9H66vVXjL`.

## Level 1

Normally you could simply type a file to *stdout* with `cat`; however, the
special character `-` is used here, so you must prefix it with `./` to indicate
that this is a local file in this current directory, i.e. `cat ./-`. This gives
the password `rRGizSaX8Mk1RTb1CNQoXTcYZWU6lgzi`.

## Level 2

Since there are spaces in the file name, you either have to surround the name by
quotations or prefix the *spaces* with a `\`. The command
`cat 'spaces in this filename'` provide the answer
`aBZ0W5EmUfAf7kHTQeOwd8bauFJ2lAiG`.

## Level 3

The password is in a hidden file. To view these on a UNIX system you append the
`-a` flag to the list command. The command `ls -a inhere` provides a listing
with the file `.hidden`. Using `cat inhere/.hidden` provides the password
`2EW7BBsr6aMMoJ2HjW067dm8EgX26xNe`.

## Level 4

The command `file` on UNIX systems can tell you the file type or its contents.
Using `find` we can run the file command on each file in the directory to see
what kind of contents they contain: `find inhere -exec file '{}' \;`. Doing this
shows that `-file07` is the only ASCII text file, thus `cat 'inhere/-file07'`
gives us `lrIWWI6bB37kxfiCQZqUdOIYfr6eEeqR`.

## Level 5

We use a similar command to the previous challenge; except this time we're
looking for a specific size that is **not** executable:
`find inhere/ -size 1033c -type f ! -executable -exec file '{}' \;`. This gives
us one file, which with the command `cat 'inhere/maybehere07/.file2'` gives
`P4L4vucdmLnm8I7Vl7jG1ApGSfjYKqJU`.

## Level 6

This time we search the entire system for a 33 byte file owned by
`bandit7:bandit6`. Since we're searching the whole file system we want to ignore
the permission denied matches by piping stderr to `/dev/null`, so:
`find / -type f -user bandit7 -group bandit6 -size 33c 2>/dev/null`. Running
`cat '/var/lib/dpkg/info/bandit7.password'` gives us
`z7WtoNQU2XfjmMtWA8u5rN4vzqu4v99S`.

## Level 7

A simple `grep` gives us the pass: `grep 'millionth' data.txt`, which is
`TESKZC0XvTetK0S9xNwm25STk5iWrBvP`.

## Level 8

You can use `uniq` with the `-u` flag to get the unique entries; however, first
you must sort the text. So the command `cat data.txt | sort | uniq -u` gives us
the password `EN632PlfYiZbn3PhVK3XOGSlNInNE00t`.

## Level 9

Here we can use `strings` to get only the human readable text out of the file,
and then a *regex* `grep` to grab only lines starting with **2** or more `=`
characters: `strings data.txt | grep -E '^={2,}'`. This gives us two lines, the
second being the password `G7w8LIi6J3kTb8A7j9LgrywtEUlyyp6s`.

## Level 10

Simply passing the file through `base64` using `-d` to decode the text gives us
`6zPeziLdR2RKNdNYFNb6nVCKzphlXHBM`.

## Level 11

The phrase "rotated by 13 positions" tells us this is just
[ROT13](https://en.wikipedia.org/wiki/ROT13). Using `tr`, we can apply the
algorithm to the text: `cat data.txt | tr 'A-Za-z' 'N-ZA-Mn-za-m'`. This gives
us `JVNBBFSmZwKKOP0XbFXOoW8chDz5yVRv`.

## Level 12

This challenge starts off with a *hexdump* file. We can use `xxd` to not only
make hexdump's, but reverse them as well. Using a combination of `file` with
piping *stdout* to *stdin* of various commands, I finally reached the point of
needing to create a temporary directory and untar the file to a directory:

```shell
bandit12@bandit:~$ xxd -r data.txt - | gunzip - | bunzip2 - | gunzip - | file -
/dev/stdin: POSIX tar archive (GNU)
```

Continuing this process several times led to the final file `data8.bin`, which
was an ASCII text file containing the password
`wbWdlBxEir4CaE8LaPhauuOo6pwRmrDw`.

## Level 13

This was just using `ssh` with the given private key to login to account 14.
Using `cat '/etc/bandit_pass/bandit14'` we get
`fGrHPx402xGC7U7rXKDaxiWFTOiF0ENq`.

## Level 14

Using *stdout* and `nc` (netcat) we can get the password:
`echo 'fGrHPx402xGC7U7rXKDaxiWFTOiF0ENq' | nc localhost 30000`, which is
`jN2kgmIXJ6fShzhT2avhotn4Zcka6tnt`.

## Level 15

Since this is sending the text via *ssl*, I took a look at the man page for the
`openssl` commands `s_client`. Just skimming through I decided to just simply
pipe the password to the command:
`echo 'jN2kgmIXJ6fShzhT2avhotn4Zcka6tnt' | openssl s_client localhost:30001`.
This just left me with the text:

```text
read R BLOCK
DONE
```

Luckily the page has a note that if you receive this you need to use `-ign_eof`.
I guess their service must not flush the connection or something? After adding
that flag, I got:

```text
read R BLOCK
Correct!
JQttfApK4SeyHwDlI9SXGR50qclOAil1
```

## Level 16

The `nmap` program can be used to port scan for various information. In
particular we can have it check for *SSL* by using the `-sV` flag; however, we
want to set the *intensity* to `1` so it doesn't try to detect for things that
aren't SSL (which is located in intensity level 1):
`nmap -sV --version-intensity 1 -p 31000-32000 localhost`. Only one port showed
up as SSL with output, and that port gave us the password private key:
`echo 'JQttfApK4SeyHwDlI9SXGR50qclOAil1' | openssl s_client -ign_eof localhost:31790`,
which was:

```text
-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAvmOkuifmMg6HL2YPIOjon6iWfbp7c3jx34YkYWqUH57SUdyJ
imZzeyGC0gtZPGujUSxiJSWI/oTqexh+cAMTSMlOJf7+BrJObArnxd9Y7YT2bRPQ
Ja6Lzb558YW3FZl87ORiO+rW4LCDCNd2lUvLE/GL2GWyuKN0K5iCd5TbtJzEkQTu
DSt2mcNn4rhAL+JFr56o4T6z8WWAW18BR6yGrMq7Q/kALHYW3OekePQAzL0VUYbW
JGTi65CxbCnzc/w4+mqQyvmzpWtMAzJTzAzQxNbkR2MBGySxDLrjg0LWN6sK7wNX
x0YVztz/zbIkPjfkU1jHS+9EbVNj+D1XFOJuaQIDAQABAoIBABagpxpM1aoLWfvD
KHcj10nqcoBc4oE11aFYQwik7xfW+24pRNuDE6SFthOar69jp5RlLwD1NhPx3iBl
J9nOM8OJ0VToum43UOS8YxF8WwhXriYGnc1sskbwpXOUDc9uX4+UESzH22P29ovd
d8WErY0gPxun8pbJLmxkAtWNhpMvfe0050vk9TL5wqbu9AlbssgTcCXkMQnPw9nC
YNN6DDP2lbcBrvgT9YCNL6C+ZKufD52yOQ9qOkwFTEQpjtF4uNtJom+asvlpmS8A
vLY9r60wYSvmZhNqBUrj7lyCtXMIu1kkd4w7F77k+DjHoAXyxcUp1DGL51sOmama
+TOWWgECgYEA8JtPxP0GRJ+IQkX262jM3dEIkza8ky5moIwUqYdsx0NxHgRRhORT
8c8hAuRBb2G82so8vUHk/fur85OEfc9TncnCY2crpoqsghifKLxrLgtT+qDpfZnx
SatLdt8GfQ85yA7hnWWJ2MxF3NaeSDm75Lsm+tBbAiyc9P2jGRNtMSkCgYEAypHd
HCctNi/FwjulhttFx/rHYKhLidZDFYeiE/v45bN4yFm8x7R/b0iE7KaszX+Exdvt
SghaTdcG0Knyw1bpJVyusavPzpaJMjdJ6tcFhVAbAjm7enCIvGCSx+X3l5SiWg0A
R57hJglezIiVjv3aGwHwvlZvtszK6zV6oXFAu0ECgYAbjo46T4hyP5tJi93V5HDi
Ttiek7xRVxUl+iU7rWkGAXFpMLFteQEsRr7PJ/lemmEY5eTDAFMLy9FL2m9oQWCg
R8VdwSk8r9FGLS+9aKcV5PI/WEKlwgXinB3OhYimtiG2Cg5JCqIZFHxD6MjEGOiu
L8ktHMPvodBwNsSBULpG0QKBgBAplTfC1HOnWiMGOU3KPwYWt0O6CdTkmJOmL8Ni
blh9elyZ9FsGxsgtRBXRsqXuz7wtsQAgLHxbdLq/ZJQ7YfzOKU4ZxEnabvXnvWkU
YOdjHdSOoKvDQNWu6ucyLRAWFuISeXw9a/9p7ftpxm0TSgyvmfLF2MIAEwyzRqaM
77pBAoGAMmjmIJdjp+Ez8duyn3ieo36yrttF5NSsJLAbxFpdlc1gvtGCWW+9Cq0b
dxviW8+TFVEBl1O4f7HVm6EpTscdDxU+bCXWkfjuRb7Dy9GOtt9JPsX8MBTakzh3
vBgsyi/sN3RqRBcGU40fOoZyfAMT8s1m/uYv52O6IgeuZ/ujbjY=
-----END RSA PRIVATE KEY-----
```

Once saving this file to `bandit17.pvk` and changing its permissions using
`chmod 700 bandit17.pvk`, I was able to login using:
`ssh -p 2220 -i ./bandit17.pvk bandit17@bandit.labs.overthewire.org`.

## Level 17

Using `diff`: `diff passwords.new passwords.old`, it showed the new entry on the
left was `hga5tuuCLF6fFzUpnagiMN8ssu9LFrdg`.

## Level 18

You can have `ssh` run commands for you by appending them to the end of the
command. Running the command:
`ssh -t -p 2220 bandit18@bandit.labs.overthewire.org 'cat readme'` gives us the
password `awhqfNnAbc1naukrpqDYcF95h7HoMTrC`. Note that I'm using `-t` to disable
pseudo-terminal allocation.

## Level 19

I think this one might have been messed up... unsure. But after messing around
with it the following got me the password:
`./bandit20-do cat /etc/bandit_pass/bandit20`, which was
`VxCazJaVykI6W36BkBU0mJTCM8rR95XT`.

## Level 20

For this one we can use `nc` to setup a server that will send *stdin* to
whomever connects. First, we create the server running:
`echo 'VxCazJaVykI6W36BkBU0mJTCM8rR95XT' | nc -l 1337 &`, this will drop the
server into the background. Then, we run `./suconnect 1337` to get the password
`NvEJF7oVjkddltPSrdKEFOllh9V1IBcq`. After sending the text, Netcat will
automatically stop (no need to kill it).

## Level 21

I assumed they'd be wanting us to look at the *cron* for `bandit22` since that's
the next user, so I ran `cat /etc/cron.d/cronjob_bandit22`, which showed me the
script `/usr/bin/cronjob_bandit22.sh`. Looking at the script, it copies the
password into `/tmp/t7O6lds9S0RqQh9aMcz6ShpAoZKF7fgv`. After catting the
contents of that file, we get `WdDozAdTM2z9DiFEQ2mGlwngMfj4EZff`.

## Level 22

The script is ran as user `bandit23`, so the `myname` variable is set to that.
Mimicking the code for `mytarget`:
`echo I am user 'bandit23' | md5sum | cut -d ' ' -f 1`, we get the *checksum*
`8ca319486bfbbc3663ea0fbe81326349`. If we run the command:
`cat /tmp/8ca319486bfbbc3663ea0fbe81326349`, we get the password
`QYw0Y2aiA672PsMmh9puTQuhoz8SyR2G`.

## Level 23

The target directory `/var/spool/bandit24` is 773, which means while we can't
*read* the contents of the folder, we can **write** to it. Looking at the
script, it runs all executable files in that folder as `bandit24` and then
deletes them. We can just simply write a script that copies the contents of the
password folder to `tmp`:

```bash
#!/bin/bash

mkdir -p '/tmp/cwb24'
cp '/etc/bandit_pass/bandit24' '/tmp/cwb24/pass.pwn'
chmod -R 775 '/tmp/cwb24'
chown -R bandit23:bandit23 '/tmp/cwb24'
```

Save that file to `pwn.sh`, make it executable via `chmod +x pwn.sh`. This gave
us a file with the password `VAfGXJ1PBSsPSnvsjI8p759leLZ9GGar`.

## Level 24

After testing it out, the format appears to be:
`VAfGXJ1PBSsPSnvsjI8p759leLZ9GGar 1234` where `1234` is any 4 digit pin code.
When sending it a bad code, we get back the text
`Wrong! Please enter the correct pincode.`. We can use this to detect if we have
a bad pin code or not, as we'll be brute forcing this since that is what the
test hints at we should do. Doing further testing, it seems it also accepts
multi-line input. This means technically I could generate a file with a list of
all possible values, then use `cat` to output it to `nc`. I created the
following script to generate the dataset (`chmod +x gen.sh`, then
`./gen.sh > dataset.dat`):

```bash
#!/bin/bash

for a in `seq 0 9`; do
        for b in `seq 0 9`; do
                for c in `seq 0 9`; do
                        for d in `seq 0 9`; do
                                printf "VAfGXJ1PBSsPSnvsjI8p759leLZ9GGar $a$b$c$d\n"
                        done
                done
        done
done
```

I then ran the following: `cat dataset.dat | nc localhost 30002`, which
eventually gave me the password `p7TaowMYrmu23Ol8hiZh9UvD0O9hpx8d`.

## Level 25

The file `/etc/passwd` contains an entry showing what shell the user uses.
Running `grep 'bandit26' /etc/passwd` gives us the shell `/usr/bin/showtext`.
When running this file, it attempts to open a file under the current user called
`text.txt`. Looking at this file, it appears to be a script that executes the
`more` command on the text file. The thing about the `more` command is if there
isn't enough room on the screen it will "pause", waiting for user input to
display more text. From there we can drop into `vi`, which will give us the
ability to run shell commands. To *pause* `more`, simply shrink the terminal
window very small and run
`ssh -p 2220 -i ./bandit26.sshkey bandit26@bandit.labs.overthewire.org` (the ssh
key is provided by user `25`). Once `more` is entered, press the `v` key to drop
into `vi`. At this point the terminal window size can be increased. Now trying
to run commands with vi using `:!` won't work as our shell is still not a proper
shell, but we can use the `set` command to set the shell, so running
`:set shell=/bin/bash` in `vi` will set our shell to *bash*. Then from there, we
can run `:!/bin/bash`, which will drop us to a *bash* shell. From here we can
then simply run `cat /etc/bandit_pass/bandit26` to give us the password
`c7GvcKlw9mC7aUQaPx7nwFstuAIBw1o1`.

## Level 26

Once dropped to the shell, simply run
`./bandit27-do cat /etc/bandit_pass/bandit27` to get the password
`YnQpBuifNMas1hcUFk70ZmqkhUU2EuaS`.

## Level 27

Here you first clone the repository using `git` (not forgetting to add the
*port* 2220 to the URI):
`git clone ssh://bandit27-git@localhost:2220/home/bandit27-git/repo`. This is of
course done in a `/tmp` directory. After `cat repo/README`, we have the password
`AVanL161y9rsbcJIsFHuw35rjaOM19nR`.

## Level 28

This is similar to above, except it is simulating someone who forgot to remove
the real password (or overwrite `git` history). Doing
`git diff bdf3099fb1fb05faa29e80ea79d9db1e29d6c9b9` gives the password
`tQKvmcwNYcFS6vmPHIUSI3ShmsrQZK8S`.

Fun fact: this happens all the time.

## Level 29

Another `git` one. This time `git diff c27fff763003bb1d57d311e6763211110b94cc87`
shows the username was `bandit29`. This didn't give much, but the phrase *no
passwords in production!* made me think about checking out if there were any
*develop* branches, so `git branch -a` showed a `dev` branch. Running
`git checkout dev` and then `cat README.md` gives the password
`xbhV3HpNGlTIdnjUrdAlPzc2L6y9EOnS`.

## Level 30

This one deals with `git` tags. To list all tags, run `git tag -l`. I jumped to
conclusions and tried to check out the tag, but the system couldn't find it, so
I decided to show the details of the tag with `git show secret` (the tags name
was *secret*), and it gave me the password `OoffzGDlzhAlerFJ2cAiz1D41JW1Mhmt`.

## Level 31

The trick to this one was to allow `key.txt` to be committed by modifying the
`.gitignore` file. The password returned from the git server was
`rmCBvG56y58BXzv98yZGdO7ATVL5dW8y`.

## Level 32

For this one I made the assumption (and a big one) that from the name this might
have been just a **C** program that was passing the input through `toupper` and
then calling `system` with it. The thing about `system` on at least most *Linux*
systems is that it just simply forks a `/bin/sh` process with the given command,
basically: `/bin/sh sh -c '[command]'`. Now, what makes this helpful is that the
shell has variables in the form of `$0` to `$9`, which are the same as arguments
passed to programs. This means that no matter what, `$0` when run by `system`
will be the name `sh`. When the text `$0` passes through `toupper`, it will stay
`$0`. That means, if we enter the text `$0` at the prompt for this shell, it
evaluates `$0` to `sh` and runs the command `sh`, dropping us to a shell. If we
run `id`, we get
`uid=11033(bandit33) gid=11032(bandit32) groups=11032(bandit32)`, meaning we are
currently user `bandit33`. We can also just run `/bin/bash` to get a normal
`bash` shell for `bandit33`.

P.S.: if you run object dump on the shell, you will see my assumptions were
correct, `objdump -d -M intel /home/bandit32/uppershell`.
