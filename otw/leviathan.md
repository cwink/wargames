# Leviathan

The following are the details for the
[Leviathan](https://overthewire.org/wargames/leviathan/) challenges.

**WARNING: These notes will contain spoilers and passwords!**

## Level 0

It seems these challenges don't really have a story or anything. So, first thing
I did was what I usually do when entering somewhere, `ls` the current directory.
There wasn't anything there, so I added the `-a` flag to see what was hidden.
There was a `.backup` folder, which usually they contain interesting things.
Inside was a `bookmarks.html` file which contained various bookmarks. Since
we're looking for a *password* I just *grepped* for `pass` via
`grep 'pass' bookmarks.html`, which gave the password `PPIfmI1qsA`.

## Level 1

In this account there is only a program called `check`. It seems to be a 64-bit
*ELF* binary, which when ran it just asks me for the password. Having a hunch
that this password may be stored **in** the binary, I dumped the object code via
`objdump -d -M intel check`:

```text
...
8049230:        68 08 a0 04 08          push   0x804a008
8049235:        e8 26 fe ff ff          call   8049060 <printf@plt>
804923a:        83 c4 10                add    esp,0x10
804923d:        e8 2e fe ff ff          call   8049070 <getchar@plt>
8049242:        88 45 dc                mov    BYTE PTR [ebp-0x24],al
8049245:        e8 26 fe ff ff          call   8049070 <getchar@plt>
804924a:        88 45 dd                mov    BYTE PTR [ebp-0x23],al
804924d:        e8 1e fe ff ff          call   8049070 <getchar@plt>
8049252:        88 45 de                mov    BYTE PTR [ebp-0x22],al
8049255:        c6 45 df 00             mov    BYTE PTR [ebp-0x21],0x0
8049259:        83 ec 08                sub    esp,0x8
804925c:        8d 45 e0                lea    eax,[ebp-0x20]
804925f:        50                      push   eax
8049260:        8d 45 dc                lea    eax,[ebp-0x24]
8049263:        50                      push   eax
8049264:        e8 d7 fd ff ff          call   8049040 <strcmp@plt>
...
```

Then I dumped the read only `data` section with `objdump -s -j .rodata check`:

```text
804a000 03000000 01000200 70617373 776f7264  ........password
804a010 3a20002f 62696e2f 73680057 726f6e67  : ./bin/sh.Wrong
804a020 20706173 73776f72 642c2047 6f6f6420   password, Good
804a030 42796520 2e2e2e00                    Bye ....
```

It seems the code here has it print out the text "password" (located at
`0x804a008`). If we load this binary up in `gdb` and put a breakpoint on
`0x8049230` via `b *0x8049230`, we can run the command `printf "%s", 0x804a008`
to get the string at this address. Placing a breakpoint on `0x8049264` and
running to it will allow us to examine the registers and stack. We can use
`i reg` for this (short for `info registers`):

```text
...
eax            0xffffd4d4          -11052
ecx            0xffffffff          -1
edx            0x804d5b3           134534579
ebx            0xf7fab000          -134565888
esp            0xffffd4c0          0xffffd4c0
...
```

At this point I just realized that this doesn't matter anymore, but just going
to leave it here since this might be useful later on. Actually, all I need to do
is just print out the two addresses that are compared with `strcmp`. They're
both *pushed* onto the stack with `push`. If we print out the second one,
`printf "%s", $ebp - 0x24` (which is `[ebp-0x24]`) we get `abc`, which was the
dummy input I provided during debugging. This means if we print the first one,
we should get the password: `printf "%s", $ebp - 0x20`:

```text
sex
```

... very funny guys... anyway, yeah, the password is `sex`. After being dropped
to a shell from the program, running `cat /etc/leviathan_pass/leviathan2` gives
us the password `mEh5PNl10e`.

## Level 2

For this one, it seems we want to have this program (`printfile`) print the
contents of `/etc/leviathan_pass/leviathan3`; however, when trying to do so, we
get "You cant have that file...". Fine. Bad grammar aside, we'll just get the
address of that string with `objdump -s -j .rodata ./printfile`:

```text
804a000 03000000 01000200 2a2a2a20 46696c65  ........*** File
804a010 20507269 6e746572 202a2a2a 00557361   Printer ***.Usa
804a020 67653a20 25732066 696c656e 616d650a  ge: %s filename.
804a030 00596f75 2063616e 74206861 76652074  .You cant have t
804a040 68617420 66696c65 2e2e2e00 2f62696e  hat file..../bin
804a050 2f636174 20257300                    /cat %s.
```

We know `Y` is *ASCII* hex `0x59`, so that means our address is `0x804a030` plus
`0x1`, making our final address `0x804a031`. This puts us at address
`0x804926a`:

```text
...
8049255:       83 ec 08                sub    esp,0x8
8049258:       6a 04                   push   0x4
804925a:       50                      push   eax
804925b:       e8 60 fe ff ff          call   80490c0 <access@plt>
8049260:       83 c4 10                add    esp,0x10
8049263:       85 c0                   test   eax,eax
8049265:       74 17                   je     804927e <main+0x98>
8049267:       83 ec 0c                sub    esp,0xc
804926a:       68 31 a0 04 08          push   0x804a031
804926f:       e8 0c fe ff ff          call   8049080 <puts@plt>
...
```

Looks like it checks the return value of `access` (never used it before, had to
look it up), which if it's `0`, you have permission, if `-1`, then no. In this
case, it uses `test eax,eax`, which basically means check the return value (on
*linux* they're stored in the `eax` or `rax` register) to see if it's `0`. We
can't really patch this file, so lets run it in `gdb`. After a `b *0x8049263`
and then a `i reg`, we can see that `eax` is indeed `-1`:

```text
...
eax            0xffffffff          -1
ecx            0xf7fbf500          -134482688
edx            0xffffffb4          -76
...
```

During this process, I noticed that the program sets the *uid* of the command
ran to the *setuid* user of the program (can be seen by running `ls`, it's the
`s` flag):

```text
...
80492a3:        e8 c8 fd ff ff          call   8049070 <geteuid@plt>
80492a8:        89 c3                   mov    ebx,eax
80492aa:        e8 c1 fd ff ff          call   8049070 <geteuid@plt>
80492af:        83 ec 08                sub    esp,0x8
80492b2:        53                      push   ebx
80492b3:        50                      push   eax
80492b4:        e8 e7 fd ff ff          call   80490a0 <setreuid@plt>
...
```

When researching the `access` function, I found a warning in the manpage about a
race condition that can occur when being used:

> Warning: Using these calls to check if a user is authorized to, for example,
> open a file before actually do‐ ing so using open(2) creates a security hole,
> because the user might exploit the short time interval between checking and
> opening the file to manipulate it. For this reason, the use of this system
> call should be avoided. (In the example just described, a safer alternative
> would be to temporarily switch the process's effective user ID to the real ID
> and then call open(2).)

Well, this just so happens to be what our program is doing. So, in theory, I
should be able to write a script that creates a file I do have permissions for,
and then before it's opened, replace it with either a copy or symbolic link to
the `leviathan3` password file:

```bash
#!/bin/bash

touch /tmp/lcw2/levpass
/home/leviathan2/printfile '/tmp/lcw2/levpass' &
rm -rf /tmp/lcw2/levpass
ln -s /etc/leviathan_pass/leviathan3 /tmp/lcw2/levpass
```

Running this gave me the password `Q0G8j4sakn`, so it worked.

## Level 3

So this one looks **exactly** like Level 1, but of course we know better.
Following the same process, we end up with a stored password of `snlprintf` at
`[ebp-0x117]`. Using it, I get a shell... no way...

Well, running `cat /etc/leviathan_pass/leviathan4` gave me `AgvropI4OA`. Which
actually works.

## Level 4

Alright, this one is a little different. It seems there's a hidden `.trash`
folder. Going into the folder we see an *elf* binary file `bin`. Running it gave
the output of:

```text
01000101 01001011 01001011 01101100 01010100 01000110 00110001 01011000 01110001 01110011 00001010
```

When I was much younger I thought it was so cool to break ASCII symbols down to
their binary values and make *hidden* messages out of them. I'm just going to go
out on a limb here and see if that's the case here:
`./bin | tr -d '[:space:]' | perl -lpe '$_=pack"B*",$_'`, which gives us
`EKKlTF1Xqs`, which looks like a password to me (and is). I found 2 `perl` one
liners online for converting to and from binary with ASCII strings that have no
spaces. The convert *to* command is `perl -lpe '$_=join " ", unpack"(B8)*"'`.

## Level 5

Running the program `leviathan5` simply tells me that it can't find the log file
`/tmp/file.log`. Now in an earlier level when peaking around I found
`/etc/cron.d/leviathan5_cleanup`, so I'm assuming that something generates that
file and this cleans it up. For the hell of it, I just created the file with
"This is some text" as its body. The system let me create `/tmp/file.log`, and
when running the program it printed the contents. I haven't dumped the binary
yet, but I have a feeling it runs as `leviathan6`. So, I ran the following:
`ln -s /etc/leviathan_pass/leviathan6 /tmp/file.log && ./leviathan5`, which gave
the output `YZ55XPVk2l`.

## Level 6

Running the program in here, I get the following output:
`usage: ./leviathan6 <4 digit code>`. I wrote a script for Level 24 during my
[bandit](bandit.md) challenge that I can re-use here for this:

```bash
#!/bin/bash

for a in `seq 0 9`; do
        for b in `seq 0 9`; do
                for c in `seq 0 9`; do
                        for d in `seq 0 9`; do
                                /home/leviathan6/leviathan6 $a$b$c$d
                        done
                done
        done
done
```

So after running `./pwn.sh | grep -v Wrong` and waiting a bit, I got impatient
and stopped it. After adding an `echo` statement, it seems the pass is `7123`.
The reason it stalled was it drops to a shell instead of spitting out a password
(I assumed wrong there). Per usual, after running
`cat /etc/leviathan_pass/leviathan7` i got the password `8GpZ5f8Hze`.

## Level 7

This was the end.
