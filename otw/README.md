# Over The Wire

Here are detailed my findings during the
[Over The Wire](https://overthewire.org/wargames/) CTF challenges.

**WARNING: These notes will contain spoilers and passwords!**

## Accessing OTW Games

Logging into the CTF servers is performed like so:

```text
ssh -p 2220 [game][level]@[game].labs.overthewire.org
```

Where `game` is the game, i.e. *bandit*, and `level` is the level number, i.e.
`0`.
